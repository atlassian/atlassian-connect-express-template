#!/usr/bin/env bash
set -e

if [ ! "$TARGET" ]; then
  echo "Must set TARGET branch env var"
  exit 1
fi

# Need to fetch rest of repo since sparse checkout
# https://stackoverflow.com/questions/6802145/how-to-convert-a-git-shallow-clone-to-a-full-clone
git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"
git fetch origin

# Creates a template based on the current branch in the specified branch
COMMIT="${BITBUCKET_COMMIT:-$(git rev-parse HEAD)}"

# Make sure the TARGET branch already exists
echo "Switching to branch $TARGET..."
git switch "$TARGET"

echo "Copying state from commit $COMMIT"
git restore --source="$COMMIT" .

# Apply build script
while read -r file; do
  if [[ $file != \#* ]]; then
    echo "Removing $file"
    # shellcheck disable=SC2086
    # We want globbing behaviour
    rm -- $file || true
  fi
done < .npmignore

echo "Committing changes..."
git add --all
if git commit -m "Build template from commit $COMMIT"
  then
    git push
  else
    echo "No changes to publish"
fi
