#!/usr/bin/env bash
# Create a zip file of the template for testing with atlas-connect
createStash=$(git stash create)
stashName="${createStash:-HEAD}" # Default to HEAD if no stash
git archive --prefix=archive/ -o template.zip "$stashName"
