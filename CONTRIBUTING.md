## Releasing new changes

See all branches: https://bitbucket.org/atlassian/atlassian-connect-express-template/branches/

When making changes, base your branch off the `develop` branch. 

Then, when your changes are merged into the `develop` branch, they will be automatically built and pushed to the correct template branch:

* `develop` -> `master` branch is for products:
  * jira
  * jira-service-desk
  * confluence
* `bitbucket` branch is for product bitbucket. 

When updating `master`, please also update `bitbucket` if neccessary.

The branches will be downloaded by: https://bitbucket.org/atlassian/atlas-connect
